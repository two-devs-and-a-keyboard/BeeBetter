package com.twodevsandakeyboard.beebetter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.twodevsandakeyboard.beebetter.lists.ItemList;

import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("beebetter")
public class BeeBetter {
	
	public static BeeBetter instance;
	public static final String modid = "beebetter";
	
	private static final Logger logger = LogManager.getLogger(modid);
	
	public BeeBetter() {
		instance = null;
		// TODO Auto-generated constructor stub
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
		MinecraftForge.EVENT_BUS.register(this);
	}

	private void setup(final FMLCommonSetupEvent event) {
		logger.info("setup method registiered.");
		
	}
	
	private void clientRegistries(final FMLClientSetupEvent event) {
		logger.info("setup method registiered.");
	}
	
	
	@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
	public	static	 class RegsitryEvents{
		@SubscribeEvent
		public static void registerItems(final RegistryEvent.Register<Item> event) {
			
			event.getRegistry().registerAll(
					
					ItemList.beebetter_honeybar = newItem("beebetter_honeybar")
					
					);
			
			
			
			logger.info("Items Registered!");
		}
		
		private static Item newItem(String name){
			return new Item(new Item.Properties().food((new Food.Builder()).hunger(4).saturation(0.3F).build()).group(ItemGroup.MISC)).setRegistryName(new ResourceLocation(modid,name));
		}
		
		}
}
